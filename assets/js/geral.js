$mobile = 812;
$url = window.location.origin;

$('.helperComplement ').remove();

var header = {
	'Accept': 'application/json',
	'REST-range': 'resources=0-100',
	'Content-Type': 'application/json; charset=utf-8'
};

var insertMasterData = function (ENT, loja, dados, fn) {
	$.ajax({
		url: '/api/dataentities/' + ENT + '/documents',
		type: 'PATCH',
		data: dados,
		headers: header,
		success: function (res) {
			fn(res);
		},
		error: function (res) {
			swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
		}
	});
};

var set_cookie = function (cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

var get_cookie = function (cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
};

var format_real = function (numero) {
	var numero = numero.toFixed(2).split('.');
	numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
	return numero.join(',');
};

var total_cart = function () {
	var quantidade = 0;
	vtexjs.checkout.getOrderForm()
		.done(function (orderForm) {

			for (var i = orderForm.items.length - 1; i >= 0; i--) {
				quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
			}

			$('#open_cart span').text(quantidade);
		});
};
total_cart();

var geral = (function () {
	var ambos = {
		add_to_cart: function () {
			$(document).on('click', '.prateleira .box_count .count', function (e) {
				e.preventDefault();

				let input = $(this).siblings('input');

				if ($(this).hasClass('count-mais')) {
					var qtd_produtos = input.attr('value');

					if (qtd_produtos) {
						qtd_produtos++;
					}
				} else if ($(this).hasClass('count-menos')) {
					var qtd_produtos = input.attr('value');

					if (qtd_produtos > 1) {
						qtd_produtos--;
					}
				}

				input.attr('value', qtd_produtos);
			});
		},

		wishlist: function () {
			if (localStorage.getItem('wishList')) {
				function products() {
					$('#list_wishlist li').remove();

					let list = localStorage.getItem('wishList').split(',');

					$(list).each(function (index, item) {
						if (item != '') {
							$.ajax({
								url: '/api/catalog_system/pub/products/search?fq=productId:' + item,
								type: 'GET'
							}).done(function (response) {
								let product = response[0];

								var por = '';
								var installments = '';
								var installmentsValue = '';

								$(product.items).each(function (index, item) {
									if (item.sellers[0].commertialOffer.AvailableQuantity != 0) {
										de = item.sellers[0].commertialOffer.ListPrice;
										por = item.sellers[0].commertialOffer.Price;
										installments = item.sellers[0].commertialOffer.Installments[0].NumberOfInstallments;
										installmentsValue = item.sellers[0].commertialOffer.Installments[0].Value;
									}
								});

								let content = '';
								content += '<li>';
								content += '<article data-id="' + product.productId + '">';
								content += '<div class="image">';
								content += '<span id="add_wishlist" class="active"><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.8894 1.98456C12.1647 1.19853 11.1702 0.765625 10.0891 0.765625C9.28096 0.765625 8.54086 1.02112 7.88931 1.52495C7.56055 1.77927 7.26265 2.09041 7 2.45357C6.73746 2.09052 6.43945 1.77927 6.11058 1.52495C5.45914 1.02112 4.71904 0.765625 3.9109 0.765625C2.82976 0.765625 1.83524 1.19853 1.11052 1.98456C0.394455 2.7614 0 3.82268 0 4.97304C0 6.15704 0.441238 7.24086 1.38855 8.38396C2.23599 9.40646 3.45396 10.4445 4.86441 11.6464C5.34602 12.0569 5.89194 12.5222 6.45879 13.0178C6.60854 13.1489 6.80069 13.2211 7 13.2211C7.1992 13.2211 7.39146 13.1489 7.541 13.018C8.10785 12.5223 8.65408 12.0568 9.13591 11.6461C10.5461 10.4444 11.7641 9.40646 12.6116 8.38385C13.5589 7.24086 14 6.15704 14 4.97293C14 3.82268 13.6055 2.7614 12.8894 1.98456Z" fill="#ADADAD"></path></svg></span>';
								content += '<a href="' + product.link + '"><img src="' + product.items[0].images[0].imageUrl + '" /></a>';
								content += '</div>';
								content += '<p class="brand"><a href="/' + product.brand + '" tabindex="-1">' + product.brand + '</a></p>';
								content += '<p class="name"><a title="' + product.productName + '" href="' + product.link + '">' + product.productName + '</a></p>';
								content += '<div class="price">';
								content += '<span class="de">' + format_real(de) + '</span>';
								content += '<span class="parcelas">Em até ' + installments + 'x <strong>' + format_real(installmentsValue) + '</strong></span>';
								content += '<span class="por">à vista por: <strong>' + format_real(por) + '</strong></span>';
								content += '</div>';
								content += '<div class="buttons">';
								content += '<div class="box_count"><a href="#" class="count count-menos" tabindex="0">-</a><input type="number" value="1" tabindex="0"><a href="#" class="count count-mais" tabindex="0">+</a></div>';
								content += '<a href="' + product.link + '" class="add_to_cart" data-id="' + product.productId + '">';
								content += '<svg width="25" height="25" viewBox="0 0 19 22" fill="#fff" xmlns="http://www.w3.org/2000/svg"><path d="M16.4513 5.5131H13.4643V4.8889C13.4643 2.75848 11.6846 1.02745 9.49988 1.02745C7.31504 1.02745 5.5353 2.75848 5.5353 4.8889V5.5131H2.54835C2.19996 5.5131 1.90607 5.76946 1.86932 6.10953L1.86932 6.10954L0.253528 21.0917L0.253526 21.0918C0.233308 21.2799 0.296119 21.4674 0.425618 21.6076L0.425661 21.6077C0.555303 21.7477 0.73963 21.8275 0.932558 21.8275H18.0669C18.26 21.8275 18.4443 21.7477 18.5738 21.6077C18.7037 21.4674 18.7663 21.2799 18.7461 21.0918L17.1303 6.10954C17.0935 5.76946 16.7996 5.5131 16.4513 5.5131ZM6.90074 4.8889C6.90074 3.49562 8.0655 2.35961 9.49988 2.35961C10.9341 2.35961 12.0989 3.49562 12.0989 4.8889V5.5131H6.90074V4.8889ZM15.8366 6.84525L17.3087 20.4953H1.69087L3.16304 6.84525H5.5353V8.15297C5.5353 8.52201 5.8423 8.81904 6.21802 8.81904C6.59374 8.81904 6.90074 8.52201 6.90074 8.15297V6.84525H12.0989V8.15297C12.0989 8.52201 12.4059 8.81904 12.7816 8.81904C13.1573 8.81904 13.4643 8.52201 13.4643 8.15297V6.84525H15.8366Z" fill="#fff" stroke="#fff" stroke-width="0.1"></path></svg>';
								content += 'Adicionar';
								content += '</a>';
								content += '</div>';
								content += '</article>';
								content += '</li>';

								$('#list_wishlist').append(content);
							});
						}
					});
				};

				if ($('body.wishlist').length) {
					products();
				}

				function list() {
					let list = localStorage.getItem('wishList').split(',');

					$(list).each(function (index, item) {
						$('#add_wishlist[data-id="' + item + '"]').addClass('active');
					});
				};
				list();
			}

			function add_wishlist() {
				//BOTÃO - PÁGINA DE PRODUTO
				if ($('body.produto').length) {
					$('.product_info #add_wishlist').attr('data-id', skuJson.productId);
				}

				$(document).on('click', '#add_wishlist', function () {
					let item = $(this).data('id') + ',';

					if ($(this).hasClass('active')) {
						//REMOVENDO
						let newWishList = localStorage.getItem('wishList').replace(item, '');

						localStorage.setItem('wishList', newWishList);

						$(this).removeClass('active');

						//IN WISHLIST
						if ($('body.wishlist').length) {
							products();
						};
					} else {
						if (localStorage.getItem('wishList')) {
							//INCLUINDO
							localStorage.setItem('wishList', localStorage.getItem('wishList') + item);
						} else {
							//INCLUINDO
							localStorage.setItem('wishList', item);
						}

						$(this).addClass('active');
					}
				});
			};
			add_wishlist();
		},

		prateleira_title: function () {
			$('.prateleira').each(function (index, item) {
				let text = $(item).find('h2').text();
				let title = $(item).prev('.prat-title').find('strong');

				title.text(text);
			});
		},

		prateleira: function () {
			if (!$('body').hasClass('departamento') && !$('body').hasClass('personal-shopper')) {
				$('.prateleira ul').slick({
					autoplay: true,
					arrows: true,
					dots: false,
					slidesToShow: 4,
					slidesToScroll: 1,
					autoplaySpeed: 4000,
					responsive: [{
							breakpoint: 1024,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1,
								variableWidth: true
							}
						},
						{
							breakpoint: 810,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1,
								variableWidth: true
							}
						}
					]
				});
			}
		},

		newsletter: function () {
			$('#newsletter').submit(function (event) {
				event.preventDefault();

				let feminino = $('#newsletter input[name="feminino"]:checked').length === 1 ? true : false;
				let masculino = $('#newsletter input[name="masculino"]:checked').length === 1 ? true : false;

				if (feminino === false && masculino === false) {
					swal('Oops', 'Escolha a categoria de produtos que deseja receber promoções!', 'warning');
				} else {
					$('#newsletter').addClass('loading');

					let obj_cliente = {
						nome: $('#newsletter input[name="name"]').val(),
						email: $('#newsletter input[name="email"]').val(),
						masculino: masculino,
						feminino: feminino
					}

					let json_cliente = JSON.stringify(obj_cliente);

					insertMasterData('NW', 'thalia', json_cliente, function (res) {
						console.log(res);

						$('#newsletter').trigger('reset');
						$('#newsletter').removeClass('loading');
						swal('Obrigado por se cadastrar', 'Em breve você receberá nossas novidades e promoções!', 'success');
					});
				}
			});
		}
	};

	var desktop = {
		todos_departamentos: function () {
			$('nav .column_1 ul li a')
				.mouseover(function () {
					$('nav .column_2 .content').removeClass('active');
					$('nav .column_2 .content.category_' + $(this).data('category')).addClass('active');
				});

			$('nav .column_2 .content')
				.mouseover(function () {
					$(this).addClass('active');
				})
				.mouseleave(function () {
					$(this).removeClass('active');
				});
		},
	};

	var mobile = {
		hamburger: function () {
			$('#hamburger').on('click', function (e) {
				e.preventDefault();
				$('header .row_2 .list .menu_1, body, #overlay').toggleClass('active');
			});

			$('#close_nav').on('click', function (e) {
				e.preventDefault();
				$('#hamburger').trigger('click');
			});
		},

		options_menu: function () {
			$('nav > .column_1 ul li a').on('click', function (e) {
				if ($(this).data('category')) {
					e.preventDefault();

					$('nav > .column_1').addClass('disabled');
					$('nav > .column_2').addClass('active');
					$('nav > .column_2 .content.category_' + $(this).data('category')).addClass('active');

					//TÍTUTLO
					let name = $(this).text();
					$('#nav-menu-back').text(name);
				}
			});

			$('#nav-menu-back').on('click', function (e) {
				e.preventDefault();
				$('nav > .column_1').removeClass('disabled');
				$('nav > .column_2, nav > .column_2 .content').removeClass('active');

				//TÍTUTLO
				$('#nav-menu-back').text('');
			});
		},

		toggle: function () {
			$('footer .toggle .title').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('ul').slideToggle();
			});
		}
	};

	ambos.add_to_cart();
	ambos.wishlist();
	ambos.prateleira_title();
	ambos.prateleira();
	ambos.newsletter();

	if ($('body').width() < $mobile) {
		//MOBILE
		mobile.hamburger();
		mobile.options_menu();
		mobile.toggle();
	} else {
		//DESKTOP
		desktop.todos_departamentos();
	}
})();

var carrinho = (function () {
	var geral = {
		toggle_carrinho: function () {
			$('header #open_cart').on('click', function (event) {
				event.preventDefault();
				$('#cart-lateral, #overlay').addClass('active');
			});

			$('#cart-lateral .header .close').on('click', function (event) {
				event.preventDefault();
				$('#cart-lateral, #overlay').removeClass('active');
			});
		}
	};

	var desktop = {
		cartLateral: function () {
			vtexjs.checkout.getOrderForm()
				.done(function (orderForm) {
					//REMOVE LOADING
					$('#cart-lateral .columns').removeClass('loading');

					//TOTAL CARRINHO
					var quantidade = 0;
					for (var i = orderForm.items.length - 1; i >= 0; i--) {
						quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
					}

					$('header #cart span').text(quantidade);

					//INFORMACOES DO CARRINHO
					if (orderForm.value != 0) {
						total_price = orderForm.value / 100;
						total_price = total_price.toFixed(2).replace('.', ',').toString();

						$('#cart-lateral .footer .total-price').text('R$: ' + total_price);
					} else {
						$('#cart-lateral .footer .total-price').text('R$: 0,00');
					}

					if (orderForm.totalizers.length != 0) {
						sub_price = orderForm.totalizers[0].value / 100;
						sub_price = sub_price.toFixed(2).replace('.', ',').toString();

						$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: ' + sub_price);

						//FRETE
						if (orderForm.totalizers[1]) {
							$('#cart-lateral .entrega .value').html('R$ ' + format_real(orderForm.totalizers[1].value));
						}
					} else {
						$('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: 0,00');
					}

					if (orderForm.items != 0) {
						total_items = orderForm.items.length;

						$('#cart-lateral .header .total-items span').text(total_items);
					} else {
						$('#cart-lateral .header .total-items span').text('0');
						$('#cart-lateral .footer .entrega .value').html('<a href="/checkout" title="Calcular">Calcular</a>')
					}
					//FIM - INFORMACOES DO CARRINHO

					//ITEMS DO CARRINHO
					$('#cart-lateral .content ul li').remove();
					for (i = 0; i < orderForm.items.length; i++) {

						price_item = orderForm.items[i].price / 100;
						price_item = price_item.toFixed(2).replace('.', ',').toString();

						var content = '';

						content += '<li data-index="' + i + '">';
						content += '<div class="column_1"><img src="' + orderForm.items[i].imageUrl + '" alt="' + orderForm.items[i].name + '"/></div>';

						content += '<div class="column_2">';
						content += '<div class="name">';
						content += '<p>' + orderForm.items[i].name + '</p>';
						content += '</div>';

						content += '<div class="ft">';
						content += '<ul>';
						content += '<li data-index="' + i + '">';
						content += '<div class="box-count">';
						content += '<a href="" class="count count-down">-</a>';
						content += '<input type="text" value="' + orderForm.items[i].quantity + '" />';
						content += '<a href="" class="count count-up">+</a>';
						content += '</div>';
						content += '</li>';
						content += '<li class="price">';
						content += '<p>R$: ' + price_item + '</p>';
						content += '</li>';
						content += '<ul>';

						content += '</div>';
						content += '</div>';

						content += '<span class="removeUni"><img src="/arquivos/ico-trash.png" alt="Remover Produto"/></span>';
						content += '</li>';

						$('#cart-lateral .content > ul').append(content);
					}
					//FIM - ITEMS DO CARRINHO
				});
		},

		changeQuantity: function () {
			$(document).on('click', '#cart-lateral .count', function (e) {
				e.preventDefault();

				$('#cart-lateral .columns').addClass('loading');

				var qtd = $(this).siblings('input[type="text"]').val();
				if ($(this).hasClass('count-up')) {
					qtd++
					$(this).siblings('input[type="text"]').removeClass('active');
					$(this).siblings('input[type="text"]').val(qtd);
				} else if ($(this).hasClass('count-down')) {
					if ($(this).siblings('input[type="text"]').val() != 1) {
						qtd--
						$(this).siblings('input[type="text"]').val(qtd);
					} else {
						//ALERTA 0 USUARIO QUANTIDADE NEGATIVA
						$(this).siblings('input[type="text"]').addClass('active');
					}
				}

				var data_index = $(this).parents('li').data('index');
				var data_quantity = $(this).parents('li').find('.box-count input[type="text"]').val();

				vtexjs.checkout.getOrderForm()
					.then(function (orderForm) {
						var total_produtos = parseInt(orderForm.items.length);
						vtexjs.checkout.getOrderForm()
							.then(function (orderForm) {
								var itemIndex = data_index;
								var item = orderForm.items[itemIndex];

								var updateItem = {
									index: data_index,
									quantity: data_quantity
								};

								return vtexjs.checkout.updateItems([updateItem], null, false);
							})
							.done(function (orderForm) {
								desktop.cartLateral();
							});
					});
			});
		},

		removeItems: function () {
			$(document).on('click', '#cart-lateral .removeUni', function () {

				var data_index = $(this).parents('li').data('index');
				var data_quantity = $(this).siblings('li').find('.box-count input[type="text"]').val();

				vtexjs.checkout.getOrderForm()
					.then(function (orderForm) {
						var itemIndex = data_index;
						var item = orderForm.items[itemIndex];
						var itemsToRemove = [{
							"index": data_index,
							"quantity": data_quantity,
						}]
						return vtexjs.checkout.removeItems(itemsToRemove);
					})
					.done(function (orderForm) {
						desktop.cartLateral();
					});
			});
		},

		removeAllItems: function () {
			$('#cart-lateral .clear').on('click', function () {
				vtexjs.checkout.removeAllItems()
					.done(function (orderForm) {
						//ATUALIZA O CARRINHO AP�S ESVAZIAR
						desktop.cartLateral();
					});
			});
		},

		btn_buy: function () {
			window.alert = function () {
				//OPEN CART
				$('header .cart-container a').trigger('click');
				desktop.cartLateral();
			}
		},

		openCart: function () {
			$('#overlay').on('click', function () {
				if ($('#cart-lateral').hasClass('active')) {
					$('#cart-lateral, #overlay').removeClass('active');
				}
			});
		}
	};

	var prateleira = {
		add_to_cart: function () {
			$(document).on('click', '.prateleira .add_to_cart', function (event) {
				event.preventDefault();

				let quantity = $(this).parents('.buttons').find('input').val();

				let item = {
					id: $(this).data('id'),
					quantity: quantity,
					seller: '1'
				};

				vtexjs.checkout.addToCart([item], null, 1)
					.done(function (orderForm) {
						//ATUALIZA CARRINHO 
						desktop.cartLateral();
						$('header #open_cart').trigger('click');
					});
			});
		}
	};

	var produto = {
		buy_in_page: function () {
			window.alert = function () {
				//ATUALIZA CARRINHO 
				desktop.cartLateral();
				$('header #open_cart').trigger('click');
			}
		}
	};

	geral.toggle_carrinho();

	desktop.cartLateral();
	desktop.changeQuantity();
	desktop.removeItems();
	desktop.removeAllItems();
	desktop.btn_buy();
	desktop.openCart();

	prateleira.add_to_cart();

	if ($('body').hasClass('produto')) {
		produto.buy_in_page();
	}
})();

var home = (function () {
	var ambos = {
		contador: function (data) {
			//EXECUÇÃO
			var countDownDate = new Date(data).getTime();

			var x = setInterval(function () {
				var now = new Date().getTime();
				var distance = countDownDate - now;

				// Time calculations for hours, minutes and seconds
				var hours = Math.floor(distance / 3600000);
				var minutes = Math.floor((distance - (hours * 3600000)) / 60000);
				var seconds = parseInt((distance - (hours * 3600000) - (minutes * 60000)) / 1000);

				$('#contador .hours').text(hours.toString().length === 1 ? '0' + hours : hours);
				$('#contador .mins').text(('0' + minutes).slice(-2));
				$('#contador .secs').text(('0' + seconds).slice(-2));

				if (distance < 0) {
					clearInterval(x);
					$('#contador ol').html('<li><i class="finish">Encerrado!</i></li>')
				}

				$('#contador:not(.active)').addClass('active');
			}, 1000);
		}
	};

	var desktop = {
		banner: function () {
			$('.home .ful_banner .desktop ul').slick({
				infinite: false,
				autoplay: true,
				arrows: true,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		}
	};

	var mobile = {
		banner: function () {
			$('.home .full_banner .mobile ul').slick({
				infinite: false,
				autoplay: true,
				arrows: false,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		},

		slick: function () {
			$('.home .list_marcas ul, .home .tipos ul').slick({
				infinite: false,
				autoplay: true,
				arrows: false,
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				variableWidth: true,
				autoplaySpeed: 3000
			});
		}
	};

	//DATA FINAL DE OFERTAS
	ambos.contador($('#contador .data_fim').text());

	if ($('body').width() < $mobile) {
		mobile.banner();
		mobile.slick();
	} else {
		desktop.banner();
	}
})();

var produto = (function () {
	var ambos = {
		share: function () {
			$('#share').attr('href', 'https://wa.me/?text=' + window.location.href);
		},

		count: function () {
			$('.produto .product_info .box_count .count').on('click', function (e) {
				e.preventDefault();

				if ($(this).hasClass('count-mais')) {
					var qtd_produtos = $('.product_info .box_count input').attr('value');

					if (qtd_produtos < skuJson.skus[0].availablequantity) {
						qtd_produtos++;
					}
				} else if ($(this).hasClass('count-menos')) {
					var qtd_produtos = $('.product_info .box_count input').attr('value');

					if (qtd_produtos > 1) {
						qtd_produtos--;
					}
				}

				$('.product_info .box_count input').attr('value', qtd_produtos);

				let qty = parseInt(qtd_produtos);
				$('.buy-in-page-quantity').trigger('quantityChanged.vtex', [skuJson.productId, qty]);
			});
		},

		toggle: function () {
			$('.produto .product_info .toggle .title').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('.content').slideToggle();
			});
		}
	};

	ambos.share();
	ambos.count();
	ambos.toggle();
})();

var departamento = (function () {
	var ambos = {
		toggle: function () {
			$('.departamento .search-multiple-navigator fieldset h5').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('div').slideToggle();
			});
		},

		view: function () {
			if ($('.vitrine_view .prateleira > ul li').length > 4) {
				$('.vitrine_view .prateleira > ul').slick({
					autoplay: true,
					arrows: true,
					dots: false,
					slidesToShow: 4,
					slidesToScroll: 1,
					autoplaySpeed: 4000,
					responsive: [{
							breakpoint: 1024,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 810,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						}
					]
				});
			}
		},

		sugestions: function () {
			var list = [];

			for (let i = 0; i < $('.prateleira ul li article').length; i++) {
				const element = $('.prateleira ul li article')[i];
				var name = $(element).attr('data-category');
				var link = $(element).attr('data-link');

				list.push({
					name: name,
					link: link
				});
			}

			function getUnique(arr, comp) {
				const unique = arr
					.map(e => e[comp])

					// store the keys of the unique objects
					.map((e, i, final) => final.indexOf(e) === i && i)

					// eliminate the dead keys & store unique objects
					.filter(e => arr[e]).map(e => arr[e]);

				return unique;
			}

			var new_list = getUnique(list, 'name');

			for (let i = 0; i < new_list.length; i++) {
				const element = new_list[i];

				$('.sugestions ul').append('<li><a href="' + element.link + '" title="' + element.name + '">' + element.name + '</a></li>');
			}
		},

		ordenacao: function () {
			$('select[name="ordenacao"]').on("change", function () {
				//REMOVE SELEÇÃO ATUAL DE ORDENAÇÃO
				$('.departamento .flags .content li[data-type="Ordenacao"]').trigger("click");

				//ATIVA NOVA ORDENAÇÃO
				$('.departamento .search-multiple-navigator label[title="' + $(this).val() + '"]').trigger("click");
			});
		},

		flags: function () {
			//ADD
			$(document).on("change", ".search-multiple-navigator label input", function () {
				var thisName = $(this).parent().text(),
					thisClass = $(this).parent().attr("title"),
					categoriaSelecionada = '<li data-name="' + thisClass + '"><div><p>' + "<strong>" + thisName + "</strong></p><i>x</i></div></li>";

				if ($(this).parent().hasClass("sr_selected")) {
					$(".flags ul").append(categoriaSelecionada);
				} else {
					$('.flags .content ul li[data-name="' + thisClass + '"]').remove();
				}

				//RESET SELECT - ORDENAÇÃO
				$('.box_ordenacao select option:first, .ordenar_mobile select option:first').prop("selected", true);
			});

			//REMOVE
			$(document).on("click", ".flags li", function (e) {
				e.preventDefault();
				$('.search-multiple-navigator label[title="' + $(this).data("name") + '"]').trigger("click");
			});

			//CLEAR
			$(".flags .btn.clear").on("click", function (e) {
				e.preventDefault();
				$(".flags .content ul li").trigger("click");
			});
		},

		smart_research: function () {
			$(".search-multiple-navigator input[type='checkbox']").vtexSmartResearch({
				shelfCallback: function () {
					console.log("shelfCallback");
				},

				ajaxCallback: function () {
					console.log("ajaxCallback");
				},
			});
		}
	};

	var mobile = {
		menu: function () {
			$('#filtrar').on('click', function () {
				$('.departamento .full_menu, #overlay').addClass('active');
			});

			$('.full_menu .btn.close, .full_menu .view.mobile').on('click', function () {
				$('.departamento .full_menu, #overlay').removeClass('active');
			});
		}
	};

	ambos.toggle();
	ambos.view();
	ambos.sugestions();
	ambos.ordenacao();
	ambos.flags();
	ambos.smart_research();

	if ($('body').width() < $mobile) {
		mobile.menu();
	}
})();

var institucional = (function () {
	var ambos = {
		question: function () {
			$('.question .title').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('.text').slideToggle();
			});
		}
	};

	ambos.question();
})();